# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

ENV VIRTUAL_ENV=/opt/venv/
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

WORKDIR /app
COPY . .

RUN pip3 install \
    --upgrade pip \
    --no-cache -r  requirements/production.txt

EXPOSE 8085

CMD ["python3", "run_service.py", "--port", "8085"]